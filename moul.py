# -*- coding: utf-8 -*-
# On import les modules nessaires
# version 0.0
import csv
from multiprocessing.sharedctypes import Value
import kivy
import sqlite3
import mysql.connector
kivy.require('1.11.1')
import pymssql
from kivy.uix.button import Button
from kivy.app import App
from kivy.lang import Builder
from kivy.uix.screenmanager import ScreenManager,FadeTransition
from kivy.uix.textinput import TextInput
from kivy.uix.slider import Slider
from kivy.uix.widget import Widget
from os.path import join, dirname
from kivy.logger import Logger
#from kivy.uix.scatter import Scatter
from kivy.properties import StringProperty
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.clock import Clock
from functools import partial
from kivy.uix.popup import Popup
from kivy.uix.label import Label
import datetime
from datetime import datetime
import automationhat
import time
from time import sleep
import socket
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from kivy.config import Config
from kivy.uix.image import Image
Config.read("/home/pi/Downloads/config.ini")
Builder.load_file('screen.kv')
conn = sqlite3.connect('/home/pi/Downloads/press.db')
def send_email_gmail(subject, message, destination):
    # First assemble the message
    msg = MIMEText(message, 'plain')
    msg['Subject'] = subject

    # Login and send the message
    port = 587
    my_mail = 'marwenrahal99@hotmail.com'
    my_password = 'Tunis2022'
    with smtplib.SMTP('smtp-mail.outlook.com', port) as server:
        server.starttls()
        server.login(my_mail, my_password)
        server.sendmail(my_mail, destination, msg.as_string())
        

send_email_gmail('Test', 'Bericht', ['marwen.rahal@bulgin.com','raed.sassi@bulgin.com'])

##############################################################
# Input Output Declaration
##############################################################

if automationhat.is_automation_hat():
    automationhat.light.power.write(1)
else:
    automationhat.light.power.write(0)

#####################################################################
# Main Screen Manager
#####################################################################
# Create the screen manager            
class Manager(AnchorLayout,BoxLayout):
    machine ='Machine Arburg 01'
    term= 0
    term_day=0
    cycle_moy = 0
    cycle_moy_day =0
    empreint_moy=0
    pilote=''
    perfermance = 0
    perfermance_day = 0
    avaibility = 0
    avaibility_day = 0
    quality = 0
    quality_day = 0
    OEE = 0
    OEE_day = 0
    run_hrs = 0
    run_hrs_day=0
    run=0
    run_day=0
    stop_hrs = 0
    stop_hrs_day=0
    stop=0
    stop_day=0  
    empreint = 0
    TRS = 0
    t_c_th=1
    t1=0
    t2=0
    hh=0
    mm=0
    ss=0
    quantitePalanifie = 0
    qtRestante=0
    qtProduite=0
    qtPurge_ajout= 0
    qtPurge = 0
    qtPurge_r=0
    qtRejet=0
    qtBon=0
    qtBon_r=0
    qtBon_r_day=0
    qtRejet_r=0
    qtRejet_r_day=0
    duree=3600
    qtRejet_ajout = 0
    qtBon_ajout = 0
    shopOrder =''
    touls =''  
    matricule =''  
    progression =0
    step = 1
    cycle = 0
    CodeArret=''
    code=''
    cycle_t=0
    matiere= ''
    start_time=time.time()
    now=datetime.now()    
    brande = ''
    shift=''
    statu='open'
    id_insert_d=1
    id_insert_h=1
    id_insert_s=1
    date = str(datetime.now())[0:19]
    ###############################################
    # Database Connection 
    ###############################################
    host ="192.168.81.8"
    username = "admin"
    password ="root"
    database ="DB Press Shop TN"
    ###############################################
    # started at finished at shop order
    ###############################################
    debut_arret=datetime.now()
    fin_arret=datetime.now()
    start = time.time()
    end = time.time()
    debut_arret_mach_off=datetime.now()
    fin_arret_mach_off=datetime.now()
	
    
    
    def __init__(self, **kwargs):       
        super(Manager, self).__init__(**kwargs)
        self.ids.managerScreen.current='loading'
        Clock.schedule_once(self.start_app,5)
                     
    def start_app(self,dt):     
        shift_time = self.now.strftime("%H")
        if shift_time in ['05','06','07','08','09','10','11','12','13']:
            self.shift = 'Shift A'
        else:
            self.shift = 'Shift B' 
        self.resume()
        self.fin_arret_mach_off=datetime.now()
        self.stop_insert(self.machine,self.debut_arret_mach_off.strftime("%e/%m/%G %H:%M:%S"),self.shopOrder,self.code,self.debut_arret.strftime("%e/%m/%G  %H:%M:%S"),self.debut_arret_mach_off.strftime("%e/%m/%G  %H:%M:%S"),self.pilote,self.shift)
        # with open('/home/pi/Downloads/report/stop.csv','a',newline='') as fichiercsv:
        #     writer=csv.writer(fichiercsv)
        #     writer.writerow([self.machine,self.debut_arret_mach_off.strftime("%e/%m/%G %H:%M:%S"),self.shopOrder,self.code,self.debut_arret.strftime("%e/%m/%G  %H:%M:%S"),self.debut_arret_mach_off.strftime("%e/%m/%G  %H:%M:%S"),self.pilote])
    
        # self.code='Machine OFF'
        # self.stop_insert(self.machine,self.fin_arret_mach_off.strftime("%e/%m/%G %H:%M:%S"),self.shopOrder,self.code,self.debut_arret_mach_off.strftime("%e/%m/%G  %H:%M:%S"),self.fin_arret_mach_off.strftime("%e/%m/%G  %H:%M:%S"),self.pilote,self.shift)
        # with open('/home/pi/Downloads/report/stop.csv','a',newline='') as fichiercsv:
        #     writer=csv.writer(fichiercsv)
        #     writer.writerow([self.machine,self.fin_arret_mach_off.strftime("%e/%m/%G %H:%M:%S"),self.shopOrder,self.code,self.debut_arret_mach_off.strftime("%e/%m/%G  %H:%M:%S"),self.fin_arret_mach_off.strftime("%e/%m/%G  %H:%M:%S"),self.pilote])
    

        
        Clock.schedule_interval(self.time,1/80.)
        Clock.schedule_interval(self.auto_save,1)
        Clock.schedule_once(self.start_comptage,5) 
        self.debut_arret=datetime.now()
        hostname=socket.gethostname()  
        print("Your Computer Name is:"+hostname) 
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #connect to any target website
        s.connect(('google.com', 0))
        ipAddress = s.getsockname()[0]
        s.close()
        print("IP Address from socket:")
        print(ipAddress)
        try:
            
            conn = pymssql.connect(server='192.168.81.8', user='admin', password='root', database='DB Press Shop TN')  
            cursor = conn.cursor()
            sql = "UPDATE IP_Address_Table_Machines SET IP_address_arburg_1 = %s "
            val = ipAddress
            cursor.execute(sql,val)
            conn.commit()

        except:
            print('ip address not added')
        
        if self.ids.managerScreen.current=='Production':
            Clock.schedule_interval(self.fermeture,1/600)
            Clock.schedule_interval(self.affiche,1/600) 
            if self.ids.Arrets.text != 'Signaler un Arrêt':
                self.code=self.ids.Arrets.text
            else:
                self.code='Arrêt non declaré'
        
        self.run=0
        self.stop=0
        self.qtBon_r=0
        self.qtBon_r_day=0
        self.qtRejet_r=0
        self.qtRejet_r_day=0
        self.cycle_moy=0
        self.cycle_moy_day=0
        self.term=0  
        self.term_day=0
        self.empreint_moy=0         

    def auto_save(self,dt):
        self.debut_arret_mach_off=datetime.now()
        self.update_data() 
        if self.ids.cas.text == 'Arrêt':
            self.ids.cas.color=(1,0,0,1)
        else:
            self.ids.cas.color=(0,255,0,0.8)
            
        
        
               
                           
    def langue(self):
         value=self.ids.lang.text
         l=['ENG','FR'] 
         for i in range(len(l)):
             if value in l[i]:
                if value=='ENG':
                    self.ids.so_l.text='Part Number'
                    self.ids.vitesse_l.text='Cycle Time'
                    self.ids.quantite_l.text='Planned Quantity'
                    self.ids.case16.text='Prev'
                    self.ids.case20.text='Next'
                    self.ids.so_p_l.text='Part Number'
                    self.ids.welcome.text='                    PRESS \n DATA COLLECTION SYSTEM'
                    self.ids.run.text='START'
                    self.ids.vitesse_p_l.text='Cycle Time'
                    self.ids.qt_p_l.text='Quantities Produced'
                    self.ids.good_p_l.text='Good Quantities'
                    self.ids.rejet_p_l.text='Rejected Quantities'
                    self.ids.reste_p_l.text='Remaining Quantities'
                    self.ids.plan_p_l.text='Quantites Planifier'
                    self.ids.Arrets.text='Signaler un Arrêt'
                    self.ids.fin_prod.text='End Of Production'
                    self.ids.retour_ll.text='Return'
                    self.ids.sub_ll.text='Submit'
                    self.ids.code_l.text='Machine Stops Code'
                    self.ids.retour_n_l.text='Return'
                elif value=='FR':
                    self.ids.welcome.text='                    PRESS \n DATA COLLECTION SYSTEM'
                    self.ids.run.text='DEBUT'
                    self.ids.so_l.text='Ordre de Fabrication'
                    self.ids.cav_l.text='Nombres des Empreintes'
                    self.ids.vitesse_l.text='Cycle'
                    self.ids.quantite_l.text='Quantite Planifier'
                    self.ids.prev_l.text='Prec'
                    self.ids.next_l.text='Suivant'
                    self.ids.so_p_l.text='Part Number'
                    self.ids.vitesse_p_l.text='Cycle'
                    self.ids.cav_p_l.text='Empreintes'
                    self.ids.qt_p_l.text='Quantites Produites'
                    self.ids.good_p_l.text='Quantites Bon'
                    self.ids.rejet_p_l.text='Quantites Rejets'
                    self.ids.reste_p_l.text='Quantites Restantes'
                    self.ids.plan_p_l.text='Quantites Planifier'
                    self.ids.Arrets.text='Signaler Arret'
                    self.ids.fin_prod.text='Fin de Production'
                    self.ids.retour_ll.text='Retour'
                    self.ids.sub_ll.text='Valider'
                    self.ids.code_l.text='Codes Arrets de Machine'
                    self.ids.retour_n_l.text='Retour'
    def pave(self):
        if self.ids.case20.text =='Abc':
            self.ids.case1.text,self.ids.case1.disabled,self.ids.case1.opacity = 'A',False,1.0
            self.ids.case2.text= 'Z'
            self.ids.case3.text= 'E'
            self.ids.case4.text= 'R'
            self.ids.case5.text,self.ids.case5.disabled,self.ids.case5.opacity = 'T',False,1.0
            self.ids.case6.text,self.ids.case6.disabled,self.ids.case6.opacity = 'Y',False,1.0
            self.ids.case7.text= 'U'
            self.ids.case8.text= 'T'
            self.ids.case9.text='I'
            self.ids.case10.text,self.ids.case10.disabled,self.ids.case10.opacity = 'O',False,1.0
            self.ids.case11.text,self.ids.case11.disabled,self.ids.case11.opacity = 'P',False,1.0
            self.ids.case12.text= 'Q'
            self.ids.case13.text= 'S'
            self.ids.case14.text='D'
            self.ids.case15.text,self.ids.case15.disabled,self.ids.case15.opacity = 'F',False,1.0
            self.ids.case16.text,self.ids.case16.disabled,self.ids.case16.opacity = 'G',False,1.0
            self.ids.case17.text= 'H'
            self.ids.case18.text= 'J'
            

            self.ids.case20.text='123'
            self.ids.case22.text,self.ids.case22.disabled,self.ids.case22.opacity = 'N',False,1.0
            self.ids.case23.text,self.ids.case23.disabled,self.ids.case23.opacity = 'M',False,1.0
            self.ids.case24.text,self.ids.case24.disabled,self.ids.case24.opacity = 'W',False,1.0
        elif self.ids.case20.text=='123':
            self.ids.case1.text,self.ids.case1.disabled,self.ids.case1.opacity = 'A',True,0.0
            self.ids.case2.text= '7'
            self.ids.case3.text= '8'
            self.ids.case4.text= '9'
            self.ids.case5.text,self.ids.case5.disabled,self.ids.case5.opacity = 'T',True,0.0
            self.ids.case6.text,self.ids.case6.disabled,self.ids.case6.opacity = 'Y',True,0.0
            self.ids.case7.text= '4'
            self.ids.case8.text= '5'
            self.ids.case9.text='6'
            self.ids.case10.text,self.ids.case10.disabled,self.ids.case10.opacity = 'O',True,0.0
            self.ids.case11.text,self.ids.case11.disabled,self.ids.case11.opacity = 'P',True,0.0
            self.ids.case12.text= '1'
            self.ids.case13.text= '2'
            self.ids.case14.text='3'
            self.ids.case15.text,self.ids.case15.disabled,self.ids.case15.opacity = 'F',True,0.0
            self.ids.case16.text,self.ids.case16.disabled,self.ids.case16.opacity = 'G',True,0.0
            self.ids.case17.text= '.'
            self.ids.case18.text= '0'
            self.ids.case19.text='DEL'

            self.ids.case20.text='Abc'
            self.ids.case22.text,self.ids.case22.disabled,self.ids.case22.opacity = 'N',True,0.0
            self.ids.case23.text,self.ids.case23.disabled,self.ids.case23.opacity = 'M',True,0.0
            self.ids.case24.text,self.ids.case24.disabled,self.ids.case24.opacity = 'W',True,0.0


 
    def bleu(self,dt):
        self.ids.Arrets.background_color=(0,0,1,1)
        Clock.unschedule(self.bleu)
        Clock.schedule_once(self.rouge,1)
    def rouge(self,dt):
        self.ids.Arrets.background_color=(1,0,0,1)
        Clock.unschedule(self.rouge)
        Clock.schedule_once(self.bleu,1)    


    def start_comptage(self,dt):
        Clock.schedule_interval(self.time_prod,1)  
    def hours_report(self,dt):
        try:
            self.run_hrs= round(self.run/3600,2)
            self.stop_hrs= round(self.stop/3600,2)
            if self.term!=0:
                cycle_mm= round(self.cycle_moy/self.term,2)
            else:
                cycle_mm = 0  
            mam= datetime.now()
            date = mam.strftime("%G/%m/%e  %H:%M")
            if self.run_hrs != 0 and self.qtBon_r != 0:
                self.perfermance = round((cycle_mm*(self.qtBon_r+self.qtRejet_r))/(self.run_hrs*3600),2)
                self.avaibility = round(self.run_hrs /(self.run_hrs+self.stop_hrs),2)
                self.quality = round(self.qtBon_r/(self.qtBon_r+self.qtRejet_r),2)
                self.OEE =  round(self.perfermance * self.avaibility * self.quality,2)
            else:
                self.perfermance = 0
                self.avaibility =0
                self.quality = 0
                self.OEE = 0 
            self.hours_insert(self.machine,date,self.shopOrder,self.run_hrs,self.stop_hrs,self.qtBon_r,self.qtRejet_r,cycle_mm,self.avaibility,self.perfermance,self.quality,self.OEE,self.empreint,self.shift)          
            # with open('/home/pi/Downloads/report/report.csv','a',newline='') as fichiercsv:
            #     writer=csv.writer(fichiercsv)
            #     print('data done')
            #     writer.writerow([self.machine,date,self.shopOrder,self.run_hrs,self.stop_hrs,self.qtBon_r,self.qtRejet_r,cycle_mm,self.avaibility,self.perfermance,self.quality,self.OEE,self.empreint])
            self.run=0
            self.stop=0
            self.qtBon_r=0
            self.qtRejet_r=0
            self.cycle_moy=0
            self.term=0         
        except:
            print('lost data hours_report')              
    def daily_report(self):
        try:
            self.run_hrs_day= round(self.run_day/3600,2)
            self.stop_hrs_day= round(self.stop_day/3600,2)
            if self.term_day!=0:
                cycle_mm= round(self.cycle_moy_day/self.term_day,2)
                empreinte= round(self.empreint_moy/self.term_day,2)
            else:
                cycle_mm = 0 
                empreinte = 0 
            mam= datetime.now()
            date = mam.strftime("%G/%m/%e  %H:%M")
            if self.run_hrs_day != 0 and self.qtBon_r_day != 0:
                self.perfermance_day = round((cycle_mm*(self.qtBon_r_day/empreinte+self.qtRejet_r_day))/(self.run_hrs_day*3600),2)
                self.avaibility_day = round(self.run_hrs_day /(self.run_hrs_day+self.stop_hrs_day),2)
                self.quality_day = round(self.qtBon_r_day/(self.qtBon_r_day+self.qtRejet_r_day),2)
                self.OEE_day =  round(self.perfermance_day * self.avaibility_day * self.quality_day,2)
            else:
                self.perfermance_day = 0
                self.avaibility_day =0
                self.quality_day = 0
                self.OEE_day = 0 
            self.daily_insert(self.machine,date,self.run_hrs_day,self.stop_hrs_day,self.qtBon_r_day,self.qtRejet_r_day,cycle_mm,self.avaibility_day,self.perfermance_day,self.quality_day,self.OEE_day,self.shift)          
            
            # with open('/home/pi/Downloads/report/report_day.csv','a',newline='') as fichiercsv:
            #     writer=csv.writer(fichiercsv)
            #     print('data done')
            #     writer.writerow([self.machine,date,self.run_hrs_day,self.stop_hrs_day,self.qtBon_r_day,self.qtRejet_r_day,empreinte,cycle_mm,self.avaibility_day,self.perfermance_day,self.quality_day,self.OEE_day])
            self.run_day=0
            self.stop_day=0
            self.qtBon_r_day=0
            self.qtRejet_r_day=0
            self.cycle_moy_day=0
            self.term_day=0         
        except:
            print('lost data')        
        
    def compteur(self,dt):
        try:              
            self.ss=self.ss+1
            if self.ss == 60:
                self.mm+=1
                self.ss=0
            elif self.mm == 60:
                self.mm=0
                self.hh+=1
            self.ids.horloge.text= 'Temps Arrêt = '+str(self.hh)+':'+str(self.mm)+':'+str(self.ss)                     
           
        except:
            print('problem python')
    def time_prod(self,dt):    
        if self.ids.cas.text == 'Production':
            self.run+=1
            self.run_day+=1
        else:
            self.stop+=1
            self.stop_day+=1 
     
    def day_data(self,dt):  
        self.daily_report()  

    def time(self,dt):
        
        mama = self.now.strftime("%H:%M")
        if mama in ['06:59','07:59','08:59','09:59','10:59','11:59','12:59','13:59']:
            self.shift = 'Shift A' 
            Clock.unschedule(self.hours_report)
            Clock.schedule_once(self.hours_report,5)      
        if mama in ['14:59','15:59','16:59','17:59','18:59','19:59','20:59','21:39']:
            self.shift = 'Shift B' 
            Clock.unschedule(self.hours_report)
            Clock.schedule_once(self.hours_report,5)                
        if mama == '13:59':
            Clock.unschedule(self.day_data)
            Clock.schedule_once(self.day_data,5)
            
        if mama == '21:44':
            Clock.unschedule(self.day_data)
            Clock.schedule_once(self.day_data,5) 
            Clock.unschedule(self.time_prod)
        self.ids.qtProduite.text= str(self.qtProduite)
        self.ids.quantite1.text = str(self.quantitePalanifie) 
        self.ids.qtBon.text= str(self.qtBon)
        self.ids.qtRejet.text = str(self.qtRejet)
        self.ids.qtRestante.text = str(self.qtRestante)     
               
    def controle(self,dt):
        self.code='Stop not declare'
        Clock.schedule_once(self.bleu,1) 
        self.fin_arret=datetime.now()
        self.stop_insert(self.machine,self.fin_arret.strftime("%e/%m/%G %H:%M:%S"),self.shopOrder,self.code,self.debut_arret.strftime("%e/%m/%G  %H:%M:%S"),self.fin_arret.strftime("%e/%m/%G  %H:%M:%S"),self.pilote,self.shift)        
        # with open('/home/pi/Downloads/report/stop.csv','a',newline='') as fichiercsv:
        #      writer=csv.writer(fichiercsv)
        #      writer.writerow([self.machine,str(self.fin_arret.strftime("%G/%m/%e  %H:%M:%S")),self.shopOrder,self.code,str(self.debut_arret.strftime("%G/%m/%e  %H:%M:%S")),str(self.fin_arret.strftime("%G/%m/%e  %H:%M:%S")),self.pilote])

        now=datetime.now()
        if self.ids.lang.text == 'ENG':
            self.ids.Arrets.text= self.code='Stop not declare'
            self.ids.cas.text='Arrêt'
            self.ids.start.text='Start at = ' + str(now.strftime("%G/%m/%e  %H:%M:%S"))
        else:
            self.ids.Arrets.text= self.code='Arret Non Déclarer'
            self.ids.cas.text='Arret'
            self.ids.start.text='Début = ' + str(now.strftime("%G/%m/%e  %H:%M:%S"))
        self.debut_arret=datetime.now()
        self.ids.Arrets.background_color= [1,0,0,1]
        self.ids.cas.color= [1,0,0,1]
        now=datetime.now()
        Clock.schedule_interval(self.compteur,1)
        self.CodeArret = '-'
        self.brande = 'production'
        Clock.unschedule(self.controle)
        self.step = 1
    ##################################################################
        # fonction de lire des entrés de donnés
    ###################################################################
    def popup_valid(self):
        box = BoxLayout(orientation = 'vertical', padding = (10))
        box.add_widget(Label(text ='Welcome   '+self.pilote,font_size=25,color= [1,0,0,1]))
        btn11 = Button(text = "OK",size_hint=[1,0.2])
        box.add_widget(btn11)
        popup = Popup(title='Press Machine', title_size= (30), 
                    title_align = 'center', content = box,
                    size_hint=(None, None),size=(600,300),
                    auto_dismiss = False)
        btn11.bind(on_press = popup.dismiss) 
        popup.open()    
    def popup_1_ENG(self):
        box = BoxLayout(orientation = 'vertical', padding = (10))
        box.add_widget(Label(text ='    invalid Data',font_size=25,color= [1,0,0,1]))
        btn11 = Button(text = "OK",size_hint=[1,0.2])
        box.add_widget(btn11)
        

        popup = Popup(title='Press Machine', title_size= (30), 
                    title_align = 'center', content = box,
                    size_hint=(None, None),background_color= [1,0,0,1],size=(600,300),
                    auto_dismiss = False)
        btn11.bind(on_press = popup.dismiss) 
        popup.open()
    def popup_1_FR(self):
        box = BoxLayout(orientation = 'vertical', padding = (10))
        box.add_widget(Label(text ='    Données invalide',font_size=25,color= [1,0,0,1]))
        btn11 = Button(text = "OK",size_hint=[1,0.2])
        box.add_widget(btn11)
        

        popup = Popup(title='Press Machine', title_size= (30), 
                    title_align = 'center', content = box,
                    size_hint=(None, None),background_color= [1,0,0,1],size=(600,300),
                    auto_dismiss = False)
        btn11.bind(on_press = popup.dismiss) 
        popup.open()    
    
        
    def popup_3_ENG(self):
        
    
        box = BoxLayout(orientation = 'vertical', padding = (10))
        box.add_widget(Label(text = 'Are you sure ??',font_size=20))
        btn1 = Button(text = "YES",size_hint=[1,1])
        btn2 = Button(text = "NO",size_hint=[1,1])
        box.add_widget(btn1)
        box.add_widget(btn2)

        popup = Popup(title='Press Machine', title_size= (30), 
                      title_align = 'center', content = box,
                      size_hint=(None, None),size=(400,200),
                      auto_dismiss = False)
        btn1.bind(on_press = self.dismiss,on_release = popup.dismiss)
        btn2.bind(on_press = popup.dismiss)
        popup.open()
    def popup_3_FR(self):
        box = BoxLayout(orientation = 'vertical', padding = (10))
        box.add_widget(Label(text = 'Vous êtes sure ??',font_size=20))
        btn1 = Button(text = "OUI",size_hint=[1,1])
        btn2 = Button(text = "NON",size_hint=[1,1])
        box.add_widget(btn1)
        box.add_widget(btn2)

        popup = Popup(title='Press Machine', title_size= (30), 
                      title_align = 'center', content = box,
                      size_hint=(None, None),size=(400,200),
                      auto_dismiss = False)
        btn1.bind(on_press = self.dismiss,on_release = popup.dismiss)
        btn2.bind(on_press = popup.dismiss)
        popup.open()
    def popup_4_ENG(self):
        
    
        box = BoxLayout(orientation = 'vertical', padding = (10))
        box.add_widget(Label(text = 'Are you sure ??',font_size=20))
        btn1 = Button(text = "YES",size_hint=[1,1])
        btn2 = Button(text = "NO",size_hint=[1,1])
        box.add_widget(btn1)
        box.add_widget(btn2)

        popup = Popup(title='Press Machine', title_size= (30), 
                      title_align = 'center', content = box,
                      size_hint=(None, None),size=(400,200),
                      auto_dismiss = False)
        btn1.bind(on_press = self.Btn,on_release = popup.dismiss)
        btn2.bind(on_press = popup.dismiss)
        popup.open()
    def reglage_mode(self):
        box = BoxLayout(orientation = 'vertical', padding = (10))
        box.add_widget(Label(text ='STOP CODE : '+self.ids.Arrets.text,font_size=31,color= [1,0,0,1],bold=True))
        btn11 = Button(text = 'END STOP',font_size=30,size_hint=[1,0.2])
        box.add_widget(btn11)
        

        popup = Popup(title=self.machine, title_size= (30), 
                    title_align = 'center', content = box,
                    size_hint=(None, None),background_color= [1,0,0,1],size=(600,300),
                    auto_dismiss = False)
        btn11.bind(on_press = self.react ,on_release = popup.dismiss) 
        popup.open()
    def react(self,event):
        Clock.unschedule(self.bleu)
        Clock.unschedule(self.rouge)
        self.fin_arret=datetime.now()
        self.stop_insert(self.machine,self.fin_arret.strftime("%e/%m/%G %H:%M:%S"),self.shopOrder,self.code,self.debut_arret.strftime("%e/%m/%G  %H:%M:%S"),self.fin_arret.strftime("%e/%m/%G  %H:%M:%S"),self.pilote,self.shift)
        # with open('/home/pi/Downloads/report/stop.csv','a',newline='') as fichiercsv:
        #     writer=csv.writer(fichiercsv)
        #     writer.writerow([self.machine,str(self.fin_arret.strftime("%G/%m/%e  %H:%M:%S")),self.shopOrder,self.code,str(self.debut_arret.strftime("%G/%m/%e  %H:%M:%S")),str(self.fin_arret.strftime("%G/%m/%e  %H:%M:%S")),self.pilote])
        self.ids.Arrets.text='Signaler un Arrêt'
        self.code='Stop not declare'
        self.ids.cas.text = 'Arrêt'
        Clock.schedule_interval(self.fermeture,1/600)
        Clock.schedule_interval(self.affiche,1/600) 
        self.debut_arret= datetime.now()      
    def dismiss(self,event):
        self.fin_arret=datetime.now()
        if self.ids.cas.text== 'Arrêt':
            self.stop_insert(self.machine,self.fin_arret.strftime("%e/%m/%G %H:%M:%S"),self.shopOrder,self.code,self.debut_arret.strftime("%e/%m/%G  %H:%M:%S"),self.fin_arret.strftime("%e/%m/%G  %H:%M:%S"),self.pilote,self.shift)
            # with open('/home/pi/Downloads/report/stop.csv','a',newline='') as fichiercsv:
            #     writer=csv.writer(fichiercsv)
            #     writer.writerow([self.machine,str(self.fin_arret.strftime("%G/%m/%e  %H:%M:%S")),self.shopOrder,self.code,str(self.debut_arret.strftime("%G/%m/%e  %H:%M:%S")),str(self.fin_arret.strftime("%G/%m/%e  %H:%M:%S")),self.pilote])
        self.statu='closed'
        self.ids.managerScreen.current='Acceuil'       
        self.end = time.time()
        self.duree = (self.end - self.start)
        self.start = time.time()  
        self.step =0
        self.duree = 3600
        Clock.unschedule(self.controle)
        self.debut_arret = datetime.now()
        Clock.unschedule(self.fermeture)
        Clock.unschedule(self.affiche)
        Clock.unschedule(self.compteur)
        self.ids.start.text=''
        self.ids.horloge.text=''
        self.hh=self.mm=self.ss=0
        self.ids.cas.size_hint = [1,1]            
        self.ids.Arrets.background_color= [0,0,1,1]
        self.ids.cas.color=[0,0,1,1]
        self.ids.cas.text=''
        self.ids.valid_reglage.text='Validation Réglage'
        self.ids.valid_reglage.background_color = (1,0,0,1)
        self.ids.valid_qualite.text='Validation Qualité'
        self.ids.valid_qualite.background_color = (1,0,0,1)
        self.ids.Arrets.text= 'Signaler un Arrêt'
        self.initialisation_var()
        
    def Btn_select_arret(self):
        self.ids.managerScreen.current = 'keyboardArret'
        Clock.unschedule(self.fermeture)
        Clock.unschedule(self.affiche)
       
    
    def Btn(self):
         
         self.fin_arret=datetime.now()
         self.stop_insert(self.machine,self.fin_arret.strftime("%e/%m/%G %H:%M:%S"),self.shopOrder,self.code,self.debut_arret.strftime("%e/%m/%G  %H:%M:%S"),self.fin_arret.strftime("%e/%m/%G  %H:%M:%S"),self.pilote,self.shift)
        #  with open('/home/pi/Downloads/report/stop.csv','a',newline='') as fichiercsv:
        #     writer=csv.writer(fichiercsv)
        #     writer.writerow([self.machine,str(self.fin_arret.strftime("%G/%m/%e  %H:%M:%S")),self.shopOrder,self.code,str(self.debut_arret.strftime("%G/%m/%e  %H:%M:%S")),str(self.fin_arret.strftime("%G/%m/%e  %H:%M:%S")),self.pilote])

         value=self.ids.Arrets.text
         l=['C/M','T/C/M','A/M','M/E','T/C','P/M','D/M','M/M','M/R','M/O','A/D','M/D','A/T/C','M/T','C/C','I/M','N/M','C/V','V/P','E/M','N/W','V/Q']

         for i in range(len(l)):
             if value in l[i]:
                self.ids.Arrets.background_color= [1,0,0,1]
                self.debut_arret=datetime.now()
                if value=='C/M':
                    self.CodeArret='2'
                    self.ids.Arrets.text='Changement matière'
                    self.brande = 'production'
                elif value=='T/C/M':
                    self.CodeArret='3'
                    self.ids.Arrets.text='Changement moule et matière'
                    self.brande = 'production'
                elif value=='A/M':
                    self.CodeArret='4'
                    self.ids.Arrets.text='Attente matière'
                    self.brande = 'matière'                   
                elif value=='M/E':
                    self.CodeArret='5'
                    self.ids.Arrets.text='Manque équipement'
                    self.brande = 'production'
                elif value=='T/C':
                    self.CodeArret='6'
                    self.ids.Arrets.text='Changement moule'
                    self.brande = 'production'
                elif value=='P/M':
                    self.CodeArret='7'
                    self.ids.Arrets.text='Problème moule au démarrage'
                    self.brande = 'production'                   
                elif value=='D/M':
                    self.CodeArret='8'
                    self.ids.Arrets.text='Démontage moule'
                    self.brande = 'production'
                elif value=='M/M':
                    self.CodeArret='9'
                    self.ids.Arrets.text='Montage moule'
                    self.brande = 'production'
                elif value=='M/R':
                    self.CodeArret='10'
                    self.ids.Arrets.text='Manque régleur'
                    self.brande = 'production'
                elif value=='M/O':
                    self.CodeArret='11'
                    self.ids.Arrets.text='Manque opérateur'
                    self.brande = 'production'
                elif value=='A/D':
                    self.CodeArret='12'
                    self.ids.Arrets.text='Attente Démarrage'
                    self.brande = 'production'
                elif value=='M/D':
                    self.CodeArret='13'
                    self.ids.Arrets.text='Manque dossier'
                    self.brande = 'production'
                elif value=='A/T/C':
                    self.CodeArret='14'
                    self.ids.Arrets.text='Attente changement moule'
                    self.brande = 'production'
                elif value=='M/T':
                    self.CodeArret='15'
                    self.ids.Arrets.text='Problème Maintenance'
                    self.brande = 'maintenance'
                elif value=='C/C':
                    self.CodeArret='16'
                    self.ids.Arrets.text='Coupure du courant'
                    self.brande = 'services généraux'
                elif value=='I/M':
                    self.CodeArret='17'
                    self.ids.Arrets.text='Intervention moule'
                    self.brande = 'mecanique'
                elif value=='N/M':
                    self.CodeArret='18'
                    self.ids.Arrets.text='Nettoyage moule'
                    self.brande = 'production'
                elif value=='C/V':
                    self.CodeArret='19'
                    self.ids.Arrets.text='Changement version'
                    self.brande = 'mecanique'
                elif value=='V/P':
                    self.CodeArret='20'
                    self.ids.Arrets.text='Validation Procès'
                    self.brande = 'process'
                elif value=='E/M':
                    self.CodeArret='21'
                    self.ids.Arrets.text='Essai moule'
                    self.brande = 'process'
                elif value=='N/W':
                    self.CodeArret='22'
                    self.ids.Arrets.text='Manque planification'
                    self.brande = 'planification'
                elif value=='V/Q':
                    self.CodeArret='23'
                    self.ids.Arrets.text='Validation Qualité'
                    self.brande = 'qualite'
                    
                self.ids.start.text=''
                self.ids.horloge.text=''
                self.hh=self.mm=self.ss=0
                Clock.unschedule(self.compteur)
                self.ids.cas.color= [1,0,0,1]
                self.ids.cas.text='Arrêt'
                now=datetime.now()
                self.ids.start.text='Start = ' + str(now.strftime("%G/%m/%e  %H:%M:%S"))
                Clock.schedule_interval(self.compteur,1)
                self.reglage_mode()                                        
                self.ids.start.text=''
                self.ids.horloge.text=''
                self.hh=self.mm=self.ss=0
                Clock.unschedule(self.compteur)
                self.ids.cas.color= [1,0,0,1]
                self.ids.cas.text='Arret'
                now=datetime.now()
                self.ids.start.text='Début = ' + str(now.strftime("%G/%m/%e  %H:%M:%S"))
                Clock.schedule_interval(self.compteur,1)
                Clock.unschedule(self.controle)
                self.ids.managerScreen.current='Production'
                Clock.schedule_interval(self.fermeture,1/600)
                Clock.schedule_interval(self.affiche,1/600)  
                self.ids.Arrets.background_color = [1,0,0,1]
            
   
    
    def active(self):
        Clock.unschedule(self.controle)
        if self.ids.cas.text=='Arrêt':
            self.fin_arret=datetime.now()
            self.stop_insert(self.machine,self.fin_arret.strftime("%e/%m/%G %H:%M:%S"),self.shopOrder,self.code,self.debut_arret.strftime("%e/%m/%G  %H:%M:%S"),self.fin_arret.strftime("%e/%m/%G  %H:%M:%S"),self.pilote,self.shift)
            # with open('/home/pi/Downloads/report/stop.csv','a',newline='') as fichiercsv:
            #     writer=csv.writer(fichiercsv)
            #     writer.writerow([self.machine,str(self.fin_arret.strftime("%G/%m/%e  %H:%M:%S")),self.shopOrder,self.code,str(self.debut_arret.strftime("%G/%m/%e  %H:%M:%S")),str(self.fin_arret.strftime("%G/%m/%e  %H:%M:%S")),self.pilote])
        self.ids.start.text=''
        self.ids.cas.text='Production'
        self.ids.cas.color=[0,1,0,1]
        Clock.unschedule(self.compteur)
        self.ids.horloge.text=''
        self.hh=0
        self.mm=0
        self.ss=0
        self.ids.Arrets.background_color= [0,0,1,1]
        
        
        if self.ids.Arrets.text != 'Signaler un Arrêt':
            if self.ids.lang.text == 'ENG':
                self.ids.Arrets.text='Signaler un Arrêt'
                self.CodeArret='-'
            elif self.ids.lang.text == 'FR':
                self.ids.Arrets.text='Signaler un Arrêt'
                self.CodeArret='-'     
        else:
            print('ok')
                
    def fermeture(self,dt) :
        
        self.now=datetime.now()
        if automationhat.input.one.is_on() and automationhat.input.two.is_off() and self.step == 1 :
             self.step = 0
             
        if automationhat.input.one.is_on() and automationhat.input.two.is_off() :
             
             self.start_time=time.time()
            
        if automationhat.input.one.is_on() and automationhat.input.two.is_on() and self.step == 0 and (time.time()-self.start_time)>0.01:
             Clock.unschedule(self.controle)
             self.active()
             
             self.step = 1
             while True:
                current_time = time.time()   
                self.cycle = round(current_time - self.t1, 2)
                self.term +=1
                self.term_day+=1
                if self.cycle > (self.cycle_t + 4):
                    self.cycle=self.cycle_t
                    self.cycle_moy+=self.cycle 
                    self.cycle_moy_day+=self.cycle 
                else:
                    self.cycle_moy+=self.cycle 
                    self.cycle_moy_day+=self.cycle   
                break
             self.t1=time.time()   
                 
           
                                
             if self.qtProduite <= self.quantitePalanifie :
                
                 
                 self.qtProduite += 1
                
                 


                 if self.ids.valid_reglage.background_color == [0,255,0,1.0]:
                    self.qtBon+= 1
                    self.qtBon_r+= 1
                    self.qtBon_r_day+= 1
                    self.qtRestante-= 1
                  
                 
                 
                    
             elif self.qtProduite > self.quantitePalanifie :
                 self.qtRestante = 0  
                 self.qtProduite += 1
                 
                 if self.ids.valid_reglage.background_color == [0,255,0,1.0]:
                    self.qtBon+= 1
                    self.qtBon_r+= 1
                    self.qtBon_r_day+= 1
                 else:
                     print('Reglage non validé')
                 
  
            
            
             self.debut_arret=datetime.now()
             Clock.schedule_once(self.controle,self.cycle_t+300)

    def affiche(self,dt): 
        
        if automationhat.input.one.is_on() and automationhat.input.two.is_on():
            self.ids.step.background_color = [0,1,0,1]
            self.ids.qtProduite.text= str(self.qtProduite)
            self.ids.quantite1.text = str(self.quantitePalanifie) 
            self.ids.qtBon.text= str(self.qtBon)
            self.ids.qtRejet.text = str(self.qtRejet)
            self.ids.qtRestante.text = str(self.qtRestante)  
            self.ids.cycle1.text=str(self.cycle)+'/'+str(self.cycle_t)
            if  self.qtProduite <= self.quantitePalanifie :
                self.ids.progression.value = self.qtProduite
                self.ids.progression.max = self.quantitePalanifie
            
                
                
          
    ######################################################################
    #initialisation
    ######################################################################
    def initialisation_var(self): 
        self.cycle = 0
        self.cycle_t = 1
        self.t1=0
        self.t2=0
        self.ids.valid_reglage.background_color= [1,0,0,1]
        self.ids.valid_reglage.color= [1,1,1,1]
        self.ids.valid_reglage.text='Validation Réglage'  
        self.ids.valid_qualite.background_color= [1,0,0,1]
        self.ids.valid_qualite.color= [1,1,1,1]
        self.ids.valid_qualite.text='Validation Qualité'  
        self.qtRejet_ajout=0
        self.qtBon_ajout = 0
        self.ids.qtProduite.text ='0'
        self.ids.qtBon.text ='0'
        self.ids.qtRejet.text ='0'
        self.quantitePalanifie = 0  
        self.qtRestante=0
        self.qtProduite=0
        self.qtRejet=0
        self.qtBon=0
        
        
        
        self.shopOrder =''
        self.touls =''
        self.progression =0
        self.ids.so.text=''
        self.ids.part.text=''
        self.ids.cav.text=''
        self.ids.mati.text=''
        self.ids.vitesse.text=''
        self.ids.quantite.text=''
        self.ids.entry1.text=''
        self.ids.entry99.text=''
        self.ids.mat.text='' 
        self.ids.pas.text=''
        self.ids.slide.index=0      
       
    ###################################################################   
    # Acceuil Screen 
    ###################################################################
        
   
    def Btn_fin_shopOrder(self):
        if self.ids.lang.text == 'ENG':
            self.popup_3_ENG()
        else:
            self.popup_3_FR()    
                
    def Btn_Valid_Reglage(self):
        self.ids.managerScreen.current ='Clavier'
        self.ids.mat.hint_text='Matricule Régleur'
        self.ids.pas.hint_text='Password Régleur'
        Clock.unschedule(self.fermeture)
        Clock.unschedule(self.affiche)  
    def Btn_Valid_qualite(self):
        self.ids.managerScreen.current ='Clavier'
        self.ids.mat.hint_text='Matricule Ag Qualité'
        self.ids.pas.hint_text='Password Ag Qualité'
        Clock.unschedule(self.fermeture)
        Clock.unschedule(self.affiche) 
    def Btn_retour_n(self):
        matricule = self.ids.mat.text
        passw = self.ids.pas.text
        if (self.ids.mat.hint_text=='Matricule Régleur'):
            cur = conn.cursor()
            statement = f"SELECT Nom from users WHERE Matricule='{matricule}' AND Password = '{passw}';"
            cur.execute(statement)
            rows = cur.fetchone()
            
            
            if not rows:  # An empty result evaluates to False.
                self.ids.managerScreen.current ='Production'
                Clock.schedule_interval(self.fermeture,1/600)
                Clock.schedule_interval(self.affiche,1/600)
                self.ids.entry99.text=''
                self.ids.mat.text='' 
                self.ids.pas.text=''
                self.ids.slide1.index=0
                if self.ids.lang.text == 'ENG':
                    self.popup_1_ENG()
                else:
                    self.popup_1_FR()    
            else:
                for row in rows:
                    self.pilote=row 
                self.ids.managerScreen.current ='Production'
                self.popup_valid()
                self.ids.entry99.text=''
                self.ids.mat.text='' 
                self.ids.pas.text=''
                self.ids.slide1.index=0
                self.ids.info.text=''
                self.ids.valid_reglage.background_color= [0,255,0,1.0]
                self.ids.valid_reglage.text= 'Réglage Validé'
                self.ids.valid_reglage.color= [0,0,0,1]
                self.ids.valid_reglage.disabled= True
                Clock.schedule_interval(self.fermeture,1/600)
                Clock.schedule_interval(self.affiche,1/600)
        else:
            cur = conn.cursor()
            statement = f"SELECT Nom from users_qualite WHERE Matricule='{matricule}' AND Password = '{passw}';"
            cur.execute(statement)
            rows = cur.fetchone()
            
            
            if not rows:  # An empty result evaluates to False.
                self.ids.managerScreen.current ='Production'
                Clock.schedule_interval(self.fermeture,1/600)
                Clock.schedule_interval(self.affiche,1/600)
                self.ids.entry99.text=''
                self.ids.mat.text='' 
                self.ids.pas.text=''
                self.ids.slide1.index=0
                if self.ids.lang.text == 'ENG':
                    self.popup_1_ENG()
                else:
                    self.popup_1_FR()    
            else:
                for row in rows:
                    self.pilote=row 
                self.ids.managerScreen.current ='Production'
                self.popup_valid()
                self.ids.entry99.text=''
                self.ids.mat.text='' 
                self.ids.pas.text=''
                self.ids.slide1.index=0
                self.ids.valid_qualite.background_color= [0,255,0,1.0]
                self.ids.valid_qualite.text= 'Qualité Validée'
                self.ids.valid_qualite.color= [0,0,0,1]
                self.ids.valid_qualite.disabled= True
                Clock.schedule_interval(self.fermeture,1/600)
                Clock.schedule_interval(self.affiche,1/600)

           
            
                
         
                 
        
                
        
        
        
        
    def Btn_retour(self):
        self.ids.managerScreen.current ='Acceuil'
        self.initialisation_var()
        
    def Btn_fail(self):
           
        if self.ids.keyboardinputInt.text == 'enter quantity Good' or 'entrer quantite Bon':
            self.ids.managerScreen.current ='Production'
            Clock.schedule_interval(self.fermeture,1/600)
            Clock.schedule_interval(self.affiche,1/600)
        if self.ids.keyboardinputInt.text == 'Enter Rejects Quantity' or 'Entrer La Quantité De Rejets' :
            self.ids.managerScreen.current ='Production'
            Clock.schedule_interval(self.fermeture,1/600)
            Clock.schedule_interval(self.affiche,1/600)     
        else:    
            self.ids.managerScreen.current ='Production'
             
                 
    ###################################################################   
    # Keybord Screen int insert
    ################################################################### 
    
    def Btn_keyboard_int(self):
        input_value = self.ids.entry3.text
        if input_value != '0' and input_value!= '':
            modifieInt = str(self.ids.keyboardinputInt.text)
            if modifieInt == 'entrer quantite Purge':
                self.qtPurge_ajout += int(input_value)
                self.qtPurge = self.qtPurge_ajout
                self.qtPurge_r=self.qtPurge_ajout
                self.ids.qtPurge.text = str(self.qtPurge)
                self.ids.managerScreen.current ='Production'
                Clock.schedule_interval(self.fermeture,1/600)
                Clock.schedule_interval(self.affiche,1/600)
                self.ids.entry3.text = ''
            if modifieInt == 'entrer quantite Bon':
                self.qtBon_ajout += int(input_value)
                self.qtRestante -= int(input_value)
                self.qtBon = self.qtBon_ajout
                self.qtBon_r=self.qtBon_ajout
                self.qtProduite = self.qtBon_ajout
                self.ids.progression.value = self.qtProduite
                self.ids.qtBon.text = str(self.qtBon)
                self.ids.qtRestante.text = str(self.qtRestante)       
                self.ids.managerScreen.current ='Production'
                Clock.schedule_interval(self.fermeture,1/600)
                Clock.schedule_interval(self.affiche,1/600)
                self.ids.entry3.text = ''
               
            elif modifieInt == 'Enter Rejects Quantity':
                self.qtRejet_ajout += int(input_value)
                self.qtRejet_r +=int(input_value)
                self.qtRejet += int(input_value)
                self.qtBon -=  int(input_value)
                # self.qtRejet += self.qtRejet_ajout
                self.qtRejet_r_day += int(input_value)
                self.ids.qtRejet.text = str(self.qtRejet)
                self.ids.managerScreen.current ='Production'
                Clock.schedule_interval(self.fermeture,1/600)
                Clock.schedule_interval(self.affiche,1/600)
                self.ids.entry3.text = ''
            elif modifieInt == 'Entrer La Quantité De Rejets':   
                self.qtRejet_ajout = int(input_value)
                self.qtRejet = self.qtRejet_ajout
                self.qtRejet_r += int(input_value)
                self.qtRejet_r_day = self.qtRejet_ajout
                self.ids.qtRejet.text = str(self.qtRejet)
                self.ids.managerScreen.current ='Production'
                Clock.schedule_interval(self.fermeture,1/600)
                Clock.schedule_interval(self.affiche,1/600)
                self.ids.entry3.text = ''    
             
            
   ###################################################################  
    #       Setting Screen Function
   ################################################################### 
         
    def Btn_ajout_rejet(self):
        Clock.unschedule(self.fermeture)
        Clock.unschedule(self.affiche)
        if self.ids.lang.text == 'ENG':
            self.ids.keyboardinputInt.text = 'Enter Rejects Quantity'
        else:
            self.ids.keyboardinputInt.text = 'Entrer La Quantité De Rejets' 
        self.ids.faza.source = 'Image/non.png'
        
        self.ids.entry3.text = ''
        self.ids.managerScreen.current ='keyboardInputNum'
    def add(self):
        self.empreint += 1
        self.ids.emp.text = str(self.empreint)
    def sous(self):
        self.empreint -= 1
        self.ids.emp.text = str(self.empreint)
    def envoyer(self):
        self.empreint = int(self.ids.emp.text)
        self.ids.empreinte1.text = self.ids.emp.text
        self.ids.managerScreen.current ='Production'
        
    def Btn_ajout_purge(self):
        Clock.unschedule(self.fermeture)
        Clock.unschedule(self.affiche)
        self.ids.keyboardinputInt.text = 'entrer quantités Purgeés'    
        self.ids.entry3.text = ''
        self.ids.managerScreen.current ='keyboardInputNum'
    def Btn_ferme_emp(self):
        self.ids.emp.text = str(self.empreint)
        self.ids.managerScreen.current ='ferme'

    def refresh(self):
        if self.ids.slide.index == 5:
            self.submit()   
        else:
            self.ids.entry1.text=''
            self.ids.slide.index += 1
            print(self.ids.slide.index)
    def unloked(self):
        self.ids.entry1.text=''
        self.ids.slide.index -= 1        
    def refresh1(self):
        if self.ids.slide1.index == 1:
            self.ids.entry99.text=''
            self.Btn_retour_n()
            self.ids.mat.text=''
            self.ids.pas.text=''   
        else:
            self.ids.entry99.text=''
            self.ids.slide1.index += 1 
    def pressed1(self):
        if (self.ids.slide1.index == 0):
            self.ids.mat.text= self.ids.entry99.text
        if (self.ids.slide1.index == 1):
            self.ids.pas.text= self.ids.entry99.text                     
    def pressed(self):
        if (self.ids.slide.index == 0):
            self.ids.so.text= self.ids.entry1.text
        if (self.ids.slide.index == 1):
            self.ids.part.text= self.ids.entry1.text    
        if (self.ids.slide.index == 2):
            self.ids.vitesse.text= self.ids.entry1.text    
        if (self.ids.slide.index == 3):
            self.ids.cav.text= self.ids.entry1.text
        if (self.ids.slide.index == 4):
            self.ids.mati.text= self.ids.entry1.text
        if (self.ids.slide.index == 5):
            self.ids.quantite.text= self.ids.entry1.text     
                   
    def Btn_run(self):
        self.ids.managerScreen.transition.direction = 'up'
        self.ids.managerScreen.current ='Setting'
        # self.langue()         
    def update_data(self):
        
        cur = conn.cursor()
        etat = (self.shopOrder,self.ids.touls1.text,self.empreint,self.quantitePalanifie,self.qtRestante,self.ids.progression.value,self.qtProduite,self.qtBon,self.qtRejet,self.cycle_t,self.qtBon_r,self.qtRejet_r,self.run_hrs,self.stop_hrs,self.ids.managerScreen.current,self.ids.Arrets.text, self.ids.start.text,self.ids.horloge.text,self.hh,self.mm,self.ss,self.debut_arret,self.ids.cas.text,self.code,self.cycle_moy,self.term,self.id_insert_d,self.id_insert_h,self.id_insert_s,self.qtRejet_r_day,self.debut_arret_mach_off,self.ids.matiere1.text, 1)
        
        cur.execute("update restore set shoporder=?, touls=?, empreint=?,quantitePalanifie=?,qtRestante=?,progression=?,qtProduite=?,qtBon=?,qtRejet=?,cycle_t=?,qtBon_r=?,qtRejet_r=?,run_hrs=?,stop_hrs=?,managerScreen=?,Arrets=?,start=?,horloge=? ,hh=?,mm=?,ss=?,start_arret=?,etat=?,code=?,cycle_moy=?,term=?,id_d=?,id_h=?,id_s=?,rejet_day=?,OFF_time=?,matiere=? where id=?", etat)
        
        conn.commit()
    def resume(self):
        cur = conn.cursor()
        statement = f"SELECT * from restore WHERE id=1;"
        cur.execute(statement)
        data = cur.fetchall()
        self.ids.ShopOrder1.text = self.shopOrder = data[0][1]
        self.ids.touls1.text=data[0][2]
        self.empreint = data[0][3]
        self.ids.empreinte1.text = str(self.empreint)
        self.quantitePalanifie = data[0][4]
        self.qtRestante = data[0][5]
        self.ids.progression.value = data[0][6]
        self.qtProduite = data[0][7]
        self.qtBon =  data[0][8]
        self.qtRejet = data[0][9]
        self.cycle_t = data[0][10]
        self.qtBon_r = data[0][11]
        self.qtRejet_r = data[0][12]
        self.run_hrs = data[0][13]
        self.stop_hrs = data[0][14]
        self.debut_arret = datetime.strptime( data[0][22], '%Y-%m-%d %H:%M:%S.%f')
        self.ids.cas.text=data[0][23]
        # self.pilote=data[0][24]
        self.code=data[0][24]
        self.cycle_moy=data[0][25]
        self.term=data[0][26]
        self.id_insert_d=data[0][27]
        self.id_insert_h=data[0][28]
        self.id_insert_s=data[0][29]
        self.qtRejet_r_day=data[0][30]
        self.debut_arret_mach_off=datetime.strptime( data[0][31], '%Y-%m-%d %H:%M:%S.%f')
        self.ids.matiere1.text = data[0][32]
        self.ids.managerScreen.current = data[0][15]
        self.ids.Arrets.text = data[0][16]
        self.ids.quantite1.text = str(self.quantitePalanifie)
        self.ids.cycle1.text=str(self.cycle)+'/'+str(self.cycle_t)
    
        if self.ids.Arrets.text != 'Signaler un Arrêt':
            self.ids.Arrets.background_color = [1,0,0,1]
            self.ids.start.text = data[0][17]
            self.ids.horloge.text = data[0][18]
            self.ids.cas.text='Arrêt'
            self.hh=data[0][19]
            self.mm=data[0][20]
            self.ss=data[0][21]
            Clock.schedule_interval(self.compteur,1)
            Clock.unschedule(self.controle)
        else:
            Clock.schedule_once(self.controle,self.cycle_t+300)
            
                
        
            
      
         
                   
                   
    def submit(self):
        if self.ids.vitesse.text == '' or self.ids.so.text == '' or self.ids.quantite.text =='':
            if self.ids.lang.text == 'ENG':
                self.popup_1_ENG()
            else:
                self.popup_1_FR()
        else:
                 
            self.ids.slide.index=0
            self.cycle_t = float(self.ids.vitesse.text)
            self.t_c_th = self.cycle_t 
            self.ids.cycle1.text=str(self.cycle)+'/'+str(self.cycle_t)
            self.statu = 'in progress'
            self.shopOrder = self.ids.so.text
            self.empreint= int(self.ids.cav.text)
            self.quantitePalanifie = int(self.ids.quantite.text)
            self.qtRestante= self.quantitePalanifie
            self.qtProduite= 0    
            self.progression =0
            self.ids.matiere1.text= self.ids.mati.text
            self.ids.empreinte1.text= str(self.empreint) 
            self.ids.touls1.text =  self.ids.part.text         
            self.ids.ShopOrder1.text = self.shopOrder
            self.ids.quantite1.text = str(self.quantitePalanifie)
            self.ids.qtRestante.text = str(self.quantitePalanifie)
            self.ids.progression.max = self.quantitePalanifie
            self.ids.progression.value = self.qtProduite
            self.ids.cycle1.text= str(self.t_c_th) 
            self.page = 1
            self.qtBon_ajout = 0
            self.qtRejet_ajout = 0
            Clock.unschedule(self.compteur)
            self.ids.start.text=''
            self.ids.horloge.text=''
            self.hh=self.mm=self.ss=0
            
            self.ids.Arrets.background_color= [0,0,1,1]    
                
            self.ids.valid_reglage.disabled=False
            self.ids.valid_qualite.disabled=False
            self.statu='in progress'     
            
            
            
            
            self.CodeArret='12'
            self.brande = 'production' 
            self.temps_technicien_fin=datetime.now()
            Clock.schedule_interval(self.fermeture,1/600)
            Clock.schedule_interval(self.affiche,1/600)
            Clock.schedule_once(self.controle,self.cycle_t+300)
            now=datetime.now()
            self.step=1   
            self.ids.managerScreen.current ='Production'
    def daily_insert(self,machine,date_d,run_hrs_d,stop_hrs_d,qtBon_d,qtRejet_d,cycle_mm_d,avaibility_d,perfermance_d,quality_d,OEE_d,shift):
        pass


        # try:
        #     mydb = mysql.connector.connect(host='localhost',
        #                             database='internal_machine_db',
        #                             user='internal_user',
        #                             password='root')
        #     mycursor = mydb.cursor()
        #     mycursor.execute('''INSERT INTO Machine_Daily_Table_CNC (Machine_Name_CNC, Date_Insert_Day_CNC, Run_Hours_Day_CNC, Down_Hours_Day_CNC,
        #                 Good_QTY_Day_CNC, Rejects_QTY_Day_CNC,Cycle_Time_Day, Availability_Rate_Day_CNC, Performance_Rate_Day_CNC, Quality_Rate_Day_CNC, OEE_Day_CNC,Shift_CNC)
        #             VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s)''',(machine,date_d,run_hrs_d,stop_hrs_d,qtBon_d,qtRejet_d,cycle_mm_d,avaibility_d,perfermance_d,quality_d,OEE_d,shift))
        #     mydb.commit()
        #     status = "data recieved"
        #     print (status)
        #     print("data recieved in the Machine daily table internal database")
        # except:
        #     status ="data not recieved "
        #     print (status)
        #     print("data not recieved in the Machine daily table internal database")
 
        
       
    def hours_insert(self,machine,date,shoporder,run_hrs,stop_hrs,qtBon_r,qtRejet_r,cycle_mm,avaibility,perfermance,quality,OEE,empreint,shift):
        pass


    #   #######--- insertion of the data in the internal database----------
    #     try:

    #         mydb = mysql.connector.connect(host='localhost',
    #                                   database='internal_machine_db',
    #                                   user='internal_user',
    #                                   password='root')
    #         mycursor = mydb.cursor()
        

    #         mycursor.execute('''INSERT INTO Real_Time_Table_CNC (Machine_Name_CNC, Date_Insert_CNC, Part_NO_CNC, Run_Hours_CNC,
    #                   Down_Hours_CNC, Good_QTY_CNC, Reject_QTY_CNC, Cycle_Time, Availability_Rate_CNC, Performance_Rate_CNC, Quality_Rate_CNC, OEE_CNC, Cavity_CNC,Shift_CNC)
    #                  VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,%s)''',(machine,date,shoporder,run_hrs,stop_hrs,qtBon_r,qtRejet_r,cycle_mm,avaibility,perfermance,quality,OEE,empreint,shift))
    #         mydb.commit()
    #         print("data recieved in the real time table internal databse")        
    #     except:

    #         print("data not recieved in the real time table internal database ")
        
#######-------  insertion data in the teable "Machine_stop_table"-------------
    def stop_insert(self,machine,date,shoporder,code,debut,fin,pilote,shift):
        pass

    
    
        # mydb = mysql.connector.connect(host='localhost',
        #                     database='internal_machine_db',
        #                     user='internal_user',
        #                     password='root')
        # mycursor = mydb.cursor()
        

        # mycursor.execute('''INSERT INTO Machine_Stop_Table_CNC (Machine_Name_CNC, Date_Insert_CNC, Part_NO_CNC, Code_Stop_CNC,
        #         Debut_Stop_CNC, Fin_Stop_Time_CNC, Regleur_Prenom_CNC,Shift_CNC)
        #         VALUES (%s, %s, %s, %s, %s, %s, %s,%s)''',(machine,date,shoporder,code,debut,fin,pilote,shift))

        # mydb.commit()
        
        # print("data recieved in the internal table Machine_Stop_Table")
           
   
    ##################################################################
    # Class Main
    ##################################################################            
class Press(App):

    def build(self):
        application = Manager()
        return application
    def on_pause(self):
        return True

if __name__ == '__main__':
    Press().run()
